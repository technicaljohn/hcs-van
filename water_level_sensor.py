#!/usr/bin/env python3
import RPi.GPIO as GPIO
import time
import automationhat as HAT

GPIO.setmode(GPIO.BCM)

tank_top_level = 14

GPIO.setup(tank_top_level, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def level_change_detected(channel):
    if channel == tank_top_level:
        if not GPIO.input(channel):
#            print("WATER - Interrupt")
            HAT.relay.three.off()
        else:
#            print("water gone - Interrupt")
            HAT.relay.three.on()


#First time through, make sure to check water level and start or stop pump accordingly
if not GPIO.input(tank_top_level):
    #print("WATER")
    # Relay is connected to "NC" or Normally Closed,
    #   which means that the Pi starts up with the pump turned on
    # This is so that if the program fails then the manual switch can intervene,
    #   and the pump still works
    # So turning the relay "on" will turn the pump off
    HAT.relay.three.off()
else:
    #print("water gone")
    HAT.relay.three.on()



# ### We're taking out this interrupt 
# GPIO.add_event_detect(tank_top_level,GPIO.BOTH, callback=level_change_detected, bouncetime=200)
# ### The above interrupt may help be more exact if we need it

#GPIO.add_event_detect(tank_top_level,GPIO.BOTH, callback=level_change_detected)

try:
#    print("Waiting for water level to change")
    while (True):
        time.sleep(7)
        # if the interrupt is not working correctly, then uncomment the block
        # ORIGINAL: Every 10 seconds do a manual check
        # The sleep command above defines how many seconds between checks

        if not GPIO.input(tank_top_level):
#             print("WATER")
             HAT.relay.three.off()
        else:
#             print("water gone")
             HAT.relay.three.on()
except KeyboardInterrupt:
    GPIO.cleanup()
else:
    GPIO.cleanup()
