#!/usr/bin/env python3
#The above is called a "shebang" and directs that this script should be run with python3
#This accomplishes a couple things, first being that it should give a hint that this script needs python3
#Second it should enable this script to be run by simply executing it, and not need to prepend it with "python3" as in "python3 DownloadSongs.py"


import automationhat as HAT
import RPi.GPIO as GPIO
from flask import Flask, render_template

# setup GPIO
GPIO.setmode(GPIO.BCM)
tank_top_level = 14 # GPIO pin connected to water level sensor
GPIO.setup(tank_top_level, GPIO.IN, pull_up_down=GPIO.PUD_UP)

app = Flask(__name__)

@app.route('/')
def index():
    pumpstate = HAT.relay.one.read()
    waterlvl = GPIO.input(tank_top_level)
    templateData = {
        'pump_state' : pumpstate,
        'water_level' : waterlvl
    }
    return render_template('index.html', **templateData)

@app.route('/pump_on/')
def pump_on():
    HAT.relay.one.off() #The relay is connected in reverse to allow for manual intervention if power is lost
    pumpstate = HAT.relay.one.read()
    waterlvl = GPIO.input(tank_top_level)
    templateData = {
        'pump_state' : pumpstate,
        'water_level' : waterlvl
    }
    return render_template('index.html', **templateData)

@app.route('/pump_off/')
def pump_off():
    HAT.relay.one.on() #The relay is connected in reverse to allow for manual intervention if power is lost
    pumpstate = HAT.relay.one.read()
    waterlvl = GPIO.input(tank_top_level)
    templateData = {
        'pump_state' : pumpstate,
        'water_level' : waterlvl
    }
    return render_template('index.html', **templateData)

@app.route('/refresh/')
def refresh():
    pumpstate = HAT.relay.one.read()
    waterlvl = GPIO.input(tank_top_level)
    templateData = {
        'pump_state' : pumpstate,
        'water_level' : waterlvl
    }
    return render_template('index.html', **templateData)


if __name__ == '__main__':
    try:
        app.run(debug=True, host='0.0.0.0')
    except KeyboardInterrupt:
        GPIO.cleanup()
    except:
        GPIO.cleanup()

